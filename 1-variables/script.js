"use strict";

let nombre = "João";
let color = "verde";
let edad = 22;

console.log(
  `Hola, me llamo ${nombre}, tengo ${edad} años y mi color favorito es el ${color}.`
);
