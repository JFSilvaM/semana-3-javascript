"use strict";

for (let horas = 0; horas < 24; horas++) {
  if (horas >= 8 && horas <= 22) {
    let cucu = [];

    for (let i = 0; i < horas; i++) {
      cucu[i] = "cucú";
    }

    console.log(`${horas} ${cucu.join(" ")}`);
  }
}
